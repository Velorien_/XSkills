﻿namespace XSkills.Web
{
    public class XSkillsConfig
    {
        public string RestrictEmailDomains { get; set; }
        public string AdminEmails { get; set; }

        public string SmtpServer { get; set; }
        public string SmtpLogin { get; set; }
        public string SmtpPassword { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpSender { get; set; }
        public bool SmtpEnableSsl { get; set; }

        public bool RequireConfirmedEmail { get; set; }
        public bool EnableAccountLockout { get; set; }
        public int MaxAttemptsUntilLockout { get; set; }
        public string ConfirmEmailFormat { get; set; }
        public string ResetPasswordEmailFormat { get; set; }
    }
}
