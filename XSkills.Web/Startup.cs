using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using XSkills.Web.Models;
using Microsoft.AspNetCore.Identity;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using XSkills.Web.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using System.IO;
using XSkills.Web.Helpers;

namespace XSkills.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {

            Configuration = configuration;
#if RELEASE
            LoadConfiguration();
#endif
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(o =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser().Build();
                o.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddDbContext<XSkillsDbContext>(o => o.UseSqlite("Filename=./XSkills.db"));
            services.AddSingleton<MailService>();
            services.AddSingleton(AnalyticsConfig.Configure());
            services.AddIdentity<XSkillsUser, XSkillsRole>(options =>
            {
                options.Password.RequiredLength = 10;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.SignIn.RequireConfirmedEmail = Configuration.GetValue<bool>("RequireConfirmedEmail");
                options.Lockout.MaxFailedAccessAttempts = Configuration.GetValue<int>("MaxAttemptsUntilLockout");
            }).AddEntityFrameworkStores<XSkillsDbContext>()
            .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.SlidingExpiration = true;
                options.LoginPath = "/Account/LogIn";
                options.LogoutPath = "/Account/LogOut";
            });

            services.Configure<XSkillsConfig>(Configuration);
        }

        public async void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStatusCodePagesWithRedirects("/Error/{0}");
            app.UseExceptionHandler(builder => builder.Run(async ctx => ctx.Response.Redirect($"/Error/500")));
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvc();

            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            using (var db = scope.ServiceProvider.GetService<XSkillsDbContext>())
            {
                db.Database.EnsureCreated();

                if (db.SkillGroups.SingleOrDefault(x => x.Name == MagicStrings.Unassigned) == null)
                    db.SkillGroups.Add(new SkillGroup { Name = MagicStrings.Unassigned });

                db.SaveChanges();

                var roleManager = scope.ServiceProvider.GetService<RoleManager<XSkillsRole>>();
                if (!await roleManager.RoleExistsAsync(MagicStrings.SuperAdmin))
                    await roleManager.CreateAsync(new XSkillsRole { Name = MagicStrings.SuperAdmin });
                if (!await roleManager.RoleExistsAsync(MagicStrings.Admin))
                    await roleManager.CreateAsync(new XSkillsRole { Name = MagicStrings.Admin });

                bool.TryParse(Configuration["DemoMode"], out var demoMode);
                if(demoMode && !db.Users.Any())
                {
                    DataFixture.LoadData(db, env.ContentRootPath);
                }
            }
        }

        public void LoadConfiguration()
        {
            Configuration["RestrictEmailDomains"] = Environment.GetEnvironmentVariable("XS_RESTRICT_EMAIL_DOMAINS");
            Configuration["AdminEmails"] = Environment.GetEnvironmentVariable("XS_ADMIN_EMAILS");
            Configuration["SmtpPassword"] = Environment.GetEnvironmentVariable("XS_SMTP_PASSWORD");
            Configuration["SmtpServer"] = Environment.GetEnvironmentVariable("XS_SMTP_SERVER");
            Configuration["SmtpPort"] = Environment.GetEnvironmentVariable("XS_SMTP_PORT");
            Configuration["SmtpLogin"] = Environment.GetEnvironmentVariable("XS_SMTP_LOGIN");
            Configuration["SmtpSender"] = Environment.GetEnvironmentVariable("XS_SMTP_SENDER");
            Configuration["SmtpEnableSsl"] = Environment.GetEnvironmentVariable("XS_SMTP_ENABLE_SSL");
            Configuration["RequireConfirmedEmail"] = Environment.GetEnvironmentVariable("XS_REQUIRE_CONFIRMED_EMAIL");
            Configuration["EnableAccountLockout"] = Environment.GetEnvironmentVariable("XS_ENABLE_ACCOUNT_LOCKOUT");
            Configuration["MaxAttemptsUntilLockout"] = Environment.GetEnvironmentVariable("XS_MAX_ATTEMPTS_UNTIL_LOCKOUT");
            Configuration["ConfirmEmailFormat"] = Environment.GetEnvironmentVariable("XS_CONFIRM_EMAIL_FORMAT");
            Configuration["ResetPasswordEmailFormat"] = Environment.GetEnvironmentVariable("XS_RESET_PASSWORD_EMAIL_FORMAT");
            Configuration["DemoMode"] = Environment.GetEnvironmentVariable("XS_DEMO_MODE");
        }
    }
}
