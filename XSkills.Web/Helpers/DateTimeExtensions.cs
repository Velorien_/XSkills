﻿using System;

namespace XSkills.Web.Helpers
{
    public static class DateTimeExtensions
    {
        public static string ToMonthsAgo(this DateTime? date)
        {
            if (date is null) return "Never";
            var months = (int)(DateTime.Today - date.Value).TotalDays / 30;
            if (months == 0) return "This month";
            return $"Over {months} month{(months == 1 ? string.Empty : "s")} ago";
        }
    }
}
