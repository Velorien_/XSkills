﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;

namespace XSkills.Web.Helpers
{
    public static class PageModelExtensions
    {
        public static void ClearModelState(this PageModel pageModel)
        {
            var keysToRemove = pageModel.ModelState.Keys.Where(x => !x.Contains('.'));
            foreach (var key in keysToRemove) pageModel.ModelState.Remove(key);
        }
    }
}
