using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;
using Microsoft.Extensions.Options;
using XSkills.Web.Services;
using System.Web;
using Microsoft.AspNetCore.Authorization;

namespace XSkills.Web.Pages.Account
{
    [AllowAnonymous]
    public class ResendActivationLinkModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly IOptions<XSkillsConfig> config;
        private readonly MailService mailService;

        public ResendActivationLinkModel(UserManager<XSkillsUser> userManager, IOptions<XSkillsConfig> config, MailService mailService)
        {
            this.userManager = userManager;
            this.config = config;
            this.mailService = mailService;
        }

        [BindProperty]
        public ResendActivationLinkFormModel Form { get; set; } = new ResendActivationLinkFormModel();

        public IActionResult OnGet()
        {
            if (!config.Value.RequireConfirmedEmail)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!config.Value.RequireConfirmedEmail)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                Form.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return Page();
            }

            var user = await userManager.FindByEmailAsync(Form.Email);
            if (user == null || await userManager.IsEmailConfirmedAsync(user))
            {
                Form.Errors.Add("Could not resend activation link.");
                return Page();
            }

            string token = await userManager.GenerateEmailConfirmationTokenAsync(user);
            string url = $"{Request.Scheme}://{Request.Host.Value}/Account/ConfirmEmail?email={user.Email}&token={HttpUtility.UrlEncode(token)}";
            Form.IsSuccess = mailService.SendEmail(user.Email, "Confirm email", string.Format(config.Value.ConfirmEmailFormat, url));
            if (!Form.IsSuccess) Form.Errors.Add("An error occured when sending email.");
            return Page();
        }
    }
}