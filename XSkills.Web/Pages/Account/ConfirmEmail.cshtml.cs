using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace XSkills.Web.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly XSkillsDbContext db;

        public ConfirmEmailModel(UserManager<XSkillsUser> userManager, XSkillsDbContext db)
        {
            this.userManager = userManager;
            this.db = db;
        }

        public List<string> Errors { get; set; } = new List<string>();

        public async Task<IActionResult> OnGet(string email, string token)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var result = await userManager.ConfirmEmailAsync(user, token);
                if (result.Succeeded)
                {
                    user.Employee = new Employee { UserId = user.Id };
                    db.Employees.Add(user.Employee);
                    await db.SaveChangesAsync();
                    await userManager.AddClaimAsync(user, new Claim("employeeId", user.Employee.Id.ToString()));
                    return Page();
                }
                else
                {
                    Errors.AddRange(result.Errors.Select(x => x.Description));
                }
            }
            else
            {
                Errors.Add("Could not find that email.");
            }

            return Page();
        }
    }
}