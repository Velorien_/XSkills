using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;
using XSkills.Web.Helpers;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using Serilog;
using Microsoft.AspNetCore.Http;
using System.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;

namespace XSkills.Web.Pages.Account
{
    public class EditModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly SignInManager<XSkillsUser> signInManager;
        private readonly XSkillsDbContext db;

        [BindProperty(SupportsGet = true)]
        public ChangeNameFormModel NameForm { get; set; } = new ChangeNameFormModel();

        [BindProperty]
        public ChangePasswordFormModel PasswordForm { get; set; } = new ChangePasswordFormModel();

        [BindProperty(SupportsGet = true)]
        public ChangeEmailFormModel EmailForm { get; set; } = new ChangeEmailFormModel();

        [BindProperty]
        public int SelectedProjectId { get; set; }

        [BindProperty]
        public string Description { get; set; }

        public string Avatar { get; set; }

        public List<string> AvatarErrors { get; set; } = new List<string>();

        public List<Project> Projects { get; set; } = new List<Project>();

        public bool ShowAdminPanel { get; set; }

        public EditModel(UserManager<XSkillsUser> userManager, SignInManager<XSkillsUser> signInManager, XSkillsDbContext db)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.db = db;
        }

        public async void OnGetAsync()
        {
            var user = await userManager.GetUserAsync(User);
            await Initialize(user, null);
        }

        public async void OnPostName()
        {
            var user = await userManager.GetUserAsync(User);
            await Initialize(user, "name");

            this.ClearModelState();
            if (!ModelState.IsValid)
            {
                NameForm.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return;
            }

            user.FirstName = NameForm.FirstName;
            user.LastName = NameForm.LastName;
            var result = await userManager.UpdateAsync(user);
            if (result.Succeeded) NameForm.IsSuccess = true;
            else NameForm.Errors.AddRange(result.Errors.Select(x => x.Description));
        }

        public async void OnPostPassword()
        {
            var user = await userManager.GetUserAsync(User);
            await Initialize(user, null);

            this.ClearModelState();
            if (!ModelState.IsValid)
            {
                PasswordForm.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return;
            }

            var result = await userManager.ChangePasswordAsync(user, PasswordForm.OldPassword, PasswordForm.NewPassword);
            if (result.Succeeded) PasswordForm.IsSuccess = true;
            else PasswordForm.Errors.AddRange(result.Errors.Select(x => x.Description));
        }

        public async Task<IActionResult> OnPostEmail()
        {
            var user = await userManager.GetUserAsync(User);
            await Initialize(user, "email");

            this.ClearModelState();
            if (!ModelState.IsValid)
            {
                EmailForm.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return Page();
            }

            user.Email = EmailForm.Email;
            user.UserName = EmailForm.Email;
            var result = await userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                await signInManager.SignOutAsync();
                return Redirect("/Account/LogIn");
            }
            else
            {
                EmailForm.Errors.AddRange(result.Errors.Select(x => x.Description));
            }

            return Page();
        }

        public async void OnPostJoinProject()
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                db.UserToProjects.Add(new UserToProject { ProjectId = SelectedProjectId, UserId = user.Id });
                db.SaveChanges();
                await Initialize(user, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to join project");
            }
        }

        public async void OnPostLeaveProject()
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                db.UserToProjects.Remove(db.UserToProjects.Single(x => x.ProjectId == SelectedProjectId && x.UserId == user.Id));
                db.SaveChanges();
                await Initialize(user, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to leave project");
            }
        }

        public async void OnPostDescription()
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                user.Description = Description;
                await userManager.UpdateAsync(user);
                await Initialize(user, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to update user profile");
            }
        }

        public async void OnPostAvatar(IFormFile avatar)
        {
            try
            {
                var user = await userManager.GetUserAsync(User);
                await Initialize(user, null);

                if (avatar == null)
                    AvatarErrors.Add("You need to choose a file.");

                if(!avatar?.ContentType.StartsWith("image") ?? false)
                    AvatarErrors.Add("You need to choose an image file.");

                if (avatar?.Length > 1024 * 1024)
                    AvatarErrors.Add("File size must be less than 1MB.");

                if (AvatarErrors.Any()) return;

                using (var stream = avatar.OpenReadStream())
                using (var outStream = new MemoryStream())
                using(var image = Image.Load(stream))
                {
                    image.Mutate(x => x.Resize(128, 128));
                    image.Save(outStream, new PngEncoder());
                    var buffer = new byte[outStream.Length];
                    outStream.Position = 0;
                    await outStream.ReadAsync(buffer, 0, (int)outStream.Length);
                    user.Avatar = Convert.ToBase64String(buffer);
                }

                db.Users.Update(user);
                db.SaveChanges();
                Avatar = user.Avatar;
            }
            catch(Exception ex)
            {
                AvatarErrors.Add("Something went wrong, please try again later.");
                Log.Error(ex, "Failed to set user avatar");
            }
        }

        private async Task Initialize(XSkillsUser user, string excludedAction)
        {
            if (excludedAction != "name") FillNameForm(user);
            if (excludedAction != "email") FillEmailForm(user);
            Description = user.Description;
            ShowAdminPanel = (await userManager.GetRolesAsync(user)).Any(x => x == MagicStrings.Admin || x == MagicStrings.SuperAdmin);
            Projects.AddRange(db.Projects.Include(x => x.UserToProjects));
            Avatar = user.Avatar;
        }

        private void FillNameForm(XSkillsUser user)
        {
            NameForm.FirstName = user.FirstName;
            NameForm.LastName = user.LastName;
        }

        private void FillEmailForm(XSkillsUser user) => EmailForm.Email = user.Email;
    }
}