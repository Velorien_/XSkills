using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;
using XSkills.Web.Services;
using Microsoft.Extensions.Options;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Microsoft.AspNetCore.Authorization;

namespace XSkills.Web.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly MailService mailService;
        private readonly IOptions<XSkillsConfig> config;

        public ForgotPasswordModel(UserManager<XSkillsUser> userManager, MailService mailService, IOptions<XSkillsConfig> config)
        {
            this.userManager = userManager;
            this.mailService = mailService;
            this.config = config;
        }

        [BindProperty]
        public ForgotPasswordFormModel Form { get; set; } = new ForgotPasswordFormModel();

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Form.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return Page();
            }

            var user = await userManager.FindByEmailAsync(Form.Email);
            if (user != null)
            {
                string token = await userManager.GeneratePasswordResetTokenAsync(user);
                string url = $"{Request.Scheme}://{Request.Host.Value}/Account/ResetPassword?email={user.Email}&token={HttpUtility.UrlEncode(token)}";
                if(!mailService.SendEmail(user.Email, "Password reset", string.Format(config.Value.ResetPasswordEmailFormat, url)))
                {
                    Form.Errors.Add("An error occured when sending email.");
                    return Page();
                }
            }

            Form.IsSuccess = true;
            return Page();
        }
    }
}