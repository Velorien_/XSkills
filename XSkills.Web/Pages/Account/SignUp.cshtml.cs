using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using XSkills.Web.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using XSkills.Web.Services;
using System.Web;

namespace XSkills.Web.Pages.Account
{
    [AllowAnonymous]
    public class SignUpModel : PageModel
    {
        private readonly UserManager<XSkillsUser> userManager;
        private readonly SignInManager<XSkillsUser> signInManager;
        private readonly RoleManager<XSkillsRole> roleManager;
        private readonly MailService mailService;
        private readonly IOptions<XSkillsConfig> config;
        private readonly XSkillsDbContext db;

        [BindProperty]
        public SignUpFormModel Form { get; set; } = new SignUpFormModel();

        public SignUpModel(UserManager<XSkillsUser> userManager,
            SignInManager<XSkillsUser> signInManager,
            RoleManager<XSkillsRole> roleManager,
            IOptions<XSkillsConfig> config,
            MailService mailService,
            XSkillsDbContext db)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.mailService = mailService;
            this.config = config;
            this.db = db;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Form.Errors.AddRange(ModelState.SelectMany(e => e.Value.Errors.Select(x => x.ErrorMessage)));
                return Page();
            }

            var allowedDomains = config.Value.RestrictEmailDomains?.Split(';', StringSplitOptions.RemoveEmptyEntries) ?? Enumerable.Empty<string>();
            string domain = Form.Email.Split('@').Last();
            if (allowedDomains.Any() && !allowedDomains.Contains(domain))
            {
                Form.Errors.Add("This email is not in an allowed domain.");
                return Page();
            }

            if(db.Users.Any(x => x.Email == Form.Email))
            {
                Form.Errors.Add("This email is already taken.");
                return Page();
            }

            var user = new XSkillsUser
            {
                FirstName = Form.FirstName,
                LastName = Form.LastName,
                Email = Form.Email,
                UserName = Form.Email,
                Avatar = MagicStrings.UnknownAvatar
            };

            var result = await userManager.CreateAsync(user, Form.Password);

            if (result.Succeeded)
            {
                user = db.Users.First(x => x.Email == Form.Email);
                var adminEmails = config.Value.AdminEmails?.Split(';', StringSplitOptions.RemoveEmptyEntries);
                if(adminEmails?.Contains(Form.Email) ?? false)
                {
                    await userManager.AddToRoleAsync(user, MagicStrings.SuperAdmin);
                }

                if (config.Value.RequireConfirmedEmail)
                {
                    string token = await userManager.GenerateEmailConfirmationTokenAsync(user);
                    string url = $"{Request.Scheme}://{Request.Host.Value}/Account/ConfirmEmail?email={user.Email}&token={HttpUtility.UrlEncode(token)}";
                    Form.IsSuccess = mailService.SendEmail(user.Email, "Confirm email", string.Format(config.Value.ConfirmEmailFormat, url));
                    if (!Form.IsSuccess) Form.Errors.Add("An error occured when sending email.");
                    return Page();
                }
                else
                {
                    var employee = new Employee { UserId = user.Id };
                    db.Employees.Add(employee);
                    await db.SaveChangesAsync();
                    user.EmployeeId = employee.Id;
                    await db.SaveChangesAsync();
                    await userManager.AddClaimAsync(user, new Claim("employeeId", employee.Id.ToString()));
                    await signInManager.SignInAsync(user, true);
                    return Redirect("/");
                }
            }

            Form.Errors.AddRange(result.Errors.Select(x => x.Description));
            return Page();
        }
    }
}