using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using XSkills.Web.Models;
using XSkills.Web.Services;

namespace XSkills.Web.Pages
{
    public class AnalyticsModel : PageModel
    {
        private readonly XSkillsDbContext db;
        private readonly AnalyticsService<XSkillsDbContext> analyticsService;

        public AnalyticsModel(XSkillsDbContext db, AnalyticsService<XSkillsDbContext> analyticsService)
        {
            this.db = db;
            this.analyticsService = analyticsService;
        }

        public List<AnalyticsMapInfo> AnalyticsTypes { get; set; }

        public Dictionary<int, string> Skills { get; set; }

        [BindProperty]
        public string SelectedAnalytics { get; set; }

        [BindProperty]
        public string SelectedSkills { get; set; }

        [BindProperty]
        public int SelectedLevel { get; set; }

        public Dictionary<string, string> DataSingle { get; set; }

        public Dictionary<string, IEnumerable<string>> DataCollection { get; set; }

        public Dictionary<AssignmentStatus, IEnumerable<NamedEmployee>> Employees { get; set; }

        public AnalyticsDefinition Analytics { get; set; }

        public void OnGet()
        {
            Initialize();
            SelectedAnalytics = AnalyticsTypes.FirstOrDefault()?.Name;
        }

        public void OnPostAnalytics()
        {
            Initialize();
            Analytics = analyticsService.GetMap(SelectedAnalytics);
            DataSingle = Analytics.EvaluateSingle(db);
            DataCollection = Analytics.EvaluateCollection(db);
        }

        public void OnPostCompetences()
        {
            Initialize();
            var selectedSkills = SelectedSkills.Split(',', System.StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x));
            Employees = db.Employees.Include(x => x.Skills).ThenInclude(x => x.Skill).Include(x => x.User)
                .Where(x => x.Skills.Any(y => selectedSkills.Contains(y.SkillId) && y.ProficiencyLevel >= SelectedLevel)).ToList()
                .GroupBy(x => x.User.AssignmentStatus).OrderBy(x => x.Key)
                .ToDictionary(x => x.Key, x => x.Select(y => NamedEmployee.FromEmployee(y)));

            foreach(var group in Employees)
            {
                foreach(var item in group.Value)
                {
                    item.Skills.RemoveAll(x => !selectedSkills.Contains(x.SkillId));
                }
            }
        }

        private void Initialize()
        {
            AnalyticsTypes = analyticsService.GetMapsInfo().ToList();
            Skills = db.Skills.OrderBy(x => x.Name).ToDictionary(x => x.Id, x => x.Name);
        }
    }
}