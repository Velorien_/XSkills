using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using XSkills.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace XSkills.Web.Pages
{
    public class ProjectModel : PageModel
    {
        private readonly XSkillsDbContext db;

        public ProjectModel(XSkillsDbContext db)
        {
            this.db = db;
        }

        public Project Project { get; set; }

        public IActionResult OnGet(int id)
        {
            Project = db.Projects
                .Include(x => x.TagToProjects).ThenInclude(x => x.Tag)
                .Include(x => x.UserToProjects).ThenInclude(x => x.User)
                .SingleOrDefault(x => x.Id == id);
            if (Project == null) return NotFound();
            return Page();
        }
    }
}