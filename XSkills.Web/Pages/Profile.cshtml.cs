using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using XSkills.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace XSkills.Web.Pages
{
    public class ProfileModel : PageModel
    {
        private readonly XSkillsDbContext db;

        public XSkillsUser CurrentUser { get; private set; }

        public ProfileModel(XSkillsDbContext db)
        {
            this.db = db;
        }

        public IActionResult OnGet(int id)
        {
            CurrentUser = db.Users.Include(x => x.UserToProjects).ThenInclude(x => x.Project)
                .Include(x => x.Employee).ThenInclude(x => x.Skills).ThenInclude(x => x.Skill).ThenInclude(x => x.SkillGroup).FirstOrDefault(x => x.Id == id);
            if (CurrentUser == null) return NotFound();
            return Page();
        }
    }
}