﻿Vue.component('xskills-editemployee', {
    template: '#xskills-editemployee-template',
    data: function () {
        return {
            selectedSkillGroup: { name: 'All', id: 0 },
            skills: [],
            selectedSkill: {},
            skillGroups: [],
            current: {},
            errors: [],
            yearsOfExperience: 0,
            proficiencyLevel: 0,
            successMessage: null,
            hideMenu: true,
            history: null,
            historyName: '',
            skillToDelete: null,
            sortOrder: null
        };
    },
    methods: {
        add: function () {
            var t = this;
            t.errors = [];
            t.validateNumericals();
            if (!t.selectedSkill || !t.selectedSkill.id) t.errors.push('You must select a skill.');
            if (t.errors.length > 0) return;

            fetch('/api/EmployeeSkills', {
                method: 'POST',
                credentials: 'include',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({ employeeId: t.employee.id, skillId: t.selectedSkill.id, yearsOfExperience: t.yearsOfExperience, proficiencyLevel: t.proficiencyLevel }),
            }).then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        t.employee.skills.push({
                            id: d.id,
                            skill: t.selectedSkill,
                            yearsOfExperience: t.yearsOfExperience,
                            proficiencyLevel: t.proficiencyLevel,
                            showTags: false
                        });
                        t.current = {};
                        t.selectedSkill = {};
                        t.displaySuccessMessage('added');
                        t.yearsOfExperience = 0;
                        t.proficiencyLevel = 0;
                    }
                    else if (r.status === 409) {
                        t.errors.push('You already have this skill.');
                    } else t.errors = d;
                });
            });
        },
        update: function () {
            var t = this;
            t.errors = [];
            if (!t.current.id) t.errors.push('You must select one of your skills.');
            t.validateNumericals();
            if (t.errors.length > 0) return;

            fetch('/api/EmployeeSkills', {
                method: 'PUT',
                credentials: 'include',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({
                    id: t.current.id,
                    employeeId: t.employee.id,
                    skillId: t.selectedSkill.id,
                    yearsOfExperience: t.yearsOfExperience,
                    proficiencyLevel: t.proficiencyLevel
                })
            }).then(r => {
                return r.json().then(d => {
                    if (r.ok) {
                        t.current.yearsOfExperience = t.yearsOfExperience;
                        t.current.proficiencyLevel = t.proficiencyLevel;
                        t.current.skill = t.selectedSkill;
                        t.current = {};
                        t.selectedSkill = {};
                        t.proficiencyLevel = 0;
                        t.yearsOfExperience = 0;
                        t.selectedSkill = t.skills[0];
                        t.displaySuccessMessage('updated');
                    } else if (r.status === 409) {
                        t.errors.push('You already have this skill.');
                    } else t.errors = d;
                });
            });
        },
        remove: function (id) {
            var t = this;
            t.errors = [];
            fetch('/api/EmployeeSkills/' + id, {
                credentials: 'include',
                method: 'DELETE'
            }).then(r => {
                if (r.ok) {
                    for ([i, s] of t.employee.skills.entries()) if (s.id === id) {
                        t.employee.skills.splice(i, 1); break;
                    }
                    t.createGroups();
                    if (t.current.id === id)
                        t.current = {};
                    t.displaySuccessMessage('deleted');
                } else t.errors.push('Could not delete the skill');
            });
        },
        displaySuccessMessage: function (verb) {
            var t = this;
            t.successMessage = 'Skill has been ' + verb + ' successfully.';
            setTimeout(() => { t.successMessage = null; }, 2000);
        },
        select: function (skill) {
            var t = this;
            t.current = skill;
            t.yearsOfExperience = skill.yearsOfExperience;
            t.proficiencyLevel = skill.proficiencyLevel;
            t.selectedSkill = skill.skill;
        },
        showHistory: function (skill) {
            var t = this;
            t.errors = [];
            fetch('/api/EmployeeSkills/Archive/' + skill.id, {
                credentials: 'include',
                method: 'GET'
            })
                .then(r => {
                    return r.json().then(d => {
                        if (r.ok) {
                            t.history = d;
                            t.historyName = skill.skill.name;
                        } else t.errors.push('Could not get history for ' + skill.skill.name);
                    });
                });
        },
        createGroups: function () {
            var t = this;
            var groups = [];
            groups.push({ name: 'All', id: 0 });
            for (s of this.skills) {
                if (groups.findIndex((e, i, self) => e.id === s.skillGroupId) < 0)
                    groups.push(s.skillGroup);
            }

            t.skillGroups = groups;
        },
        validateNumericals: function () {
            if (this.yearsOfExperience === '' || isNaN(this.yearsOfExperience))
                this.errors.push("Years of experience must be a number");
            if (this.proficiencyLevel === '' || isNaN(this.proficiencyLevel))
                this.errors.push("Proficiency level must be a number");
        },
        employeeHasSkill: function (skill) {
            for (s of this.employee.skills) {
                if (s.skill.id === skill.id) return true;
            }

            return false;
        },
        sortByName: function (dir) {
            this.sortOrder = function (a, b) {
                var nameA = a.skill.name.toLowerCase();
                var nameB = b.skill.name.toLowerCase();
                if (nameA < nameB) return -1 * dir;
                if (nameA > nameB) return 1 * dir;
                return 0;
            };
        },
        sortByNumber: function (dir, number) {
            this.sortOrder = function (a, b) {
                return (number(a) - number(b)) * dir;
            };
        },
    },
    props: ['employee'],
    watch: {
        selectedSkillGroup: function () {
            this.errors = [];
            this.current = {};
            this.selectedSkill = {};
            this.yearsOfExperience = 0;
            this.proficiencyLevel = 0;
        }
    },
    computed: {
        sortedSkills: function () {
            var s = this.employee.skills;
            if (!this.sortOrder) this.sortByName(1);
            s.sort(this.sortOrder);
            return s;
        }
    },
    mounted: function () {
        var t = this;
        fetch('/api/Skills', {
            credentials: 'include',
            method: 'GET'
        }).then(r => { if (r.ok) return r.json(); })
            .then(d => {
                if (d && d.length) {
                    t.skills = d;
                    t.createGroups();
                }
            });
    }
});