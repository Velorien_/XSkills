﻿using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace XSkills.Web.Services
{
    public class MailService
    {
        private readonly string smtpServer;
        private readonly int smtpPort;
        private readonly string smtpSender;
        private readonly string smtpLogin;
        private readonly string smtpPassword;
        private readonly bool useSsl;

        public MailService(IOptions<XSkillsConfig> config)
        {
            smtpPassword = config.Value.SmtpPassword;
            smtpLogin = config.Value.SmtpLogin;
            smtpServer = config.Value.SmtpServer;
            smtpSender = config.Value.SmtpSender;
            smtpPort = config.Value.SmtpPort;
            useSsl = config.Value.SmtpEnableSsl;
        }

        public bool SendEmail(string recipient, string subject, string body, string subjectDisplayName = null)
        {
            using (var client = new SmtpClient(smtpServer, smtpPort))
            {
                client.Credentials = new NetworkCredential(smtpLogin, smtpPassword);
                try
                {
                    var message = new MailMessage
                    {
                        From = new MailAddress(smtpSender, "XSkills"),
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = true,
                    };
                    message.To.Add(new MailAddress(recipient, subjectDisplayName ?? recipient));

                    client.EnableSsl = useSsl;
                    client.Send(message);
                    return true;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Sending email failed");
                    return false;
                }
            }
        }
    }
}
