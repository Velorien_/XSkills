﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using XSkills.Web.Models;
using System.Collections.Generic;
using Serilog;

namespace XSkills.Web.Api
{
    [Route("api/EmployeeSkills")]
    public class EmployeeSkillsController : CrudController<EmployeeSkill>
    {
        public EmployeeSkillsController(XSkillsDbContext db) : base(db) { }

        [NonAction]
        public override IEnumerable<EmployeeSkill> Get() => default;

        public override IActionResult Post([FromBody] EmployeeSkill skill)
        {
            if (skill == null) return BadRequest(new[] { "Bad data format." });
            if (skill.EmployeeId.ToString() != User.Claims.Single(x => x.Type == "employeeId").Value) return Unauthorized();
            if (skill.Id != 0) return BadRequest(new[] { "This endpoint is insert-only. Try to POST instead." });
            if (skill.SkillId == 0 && skill.Skill == null) return BadRequest(new[] { "You must select a skill." });
            if (!ModelState.IsValid) return BadRequest(ModelState.SelectMany(x => x.Value.Errors.Select(y => y.ErrorMessage)));

            try
            {
                skill.Archive.Add(new SkillChange
                {
                    NewProficiencyLevel = skill.ProficiencyLevel,
                    NewYearsOfExperience = skill.YearsOfExperience
                });

                var employee = db.Employees.SingleOrDefault(x => x.Id == skill.EmployeeId);
                if (employee != null) employee.LastSkillUpdate = DateTime.Now;
                else return BadRequest(new[] { $"There is no such employee with id {skill.EmployeeId}." });

                db.EmployeeSkills.Add(skill);
                db.Update(employee);
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Error(ex, "Db insert failed");
                return StatusCode(409, new[] { "Entity conflict detected." });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Db insert failed");
                return BadRequest(new[] { $"DB Update error: {ex.Message}" });
            }

            return Ok(skill);
        }

        public override IActionResult Put([FromBody] EmployeeSkill skill)
        {
            if (skill == null) return BadRequest(new[] { "Bad data format." });
            if (skill.EmployeeId.ToString() != User.Claims.Single(x => x.Type == "employeeId").Value) return Unauthorized();
            if (skill.Id == 0) return BadRequest(new[] { "This endpoint is update-only. Try to POST instead." });
            if(!ModelState.IsValid) return BadRequest(ModelState.SelectMany(x => x.Value.Errors.Select(y => y.ErrorMessage)));
            var skillFromDb = db.EmployeeSkills.Include(x => x.Archive).SingleOrDefault(x => x.Id == skill.Id);
            if (skillFromDb == null) return NotFound(new[] { "There is no such employee skill." });
            if (skillFromDb.SkillId != skill.SkillId) return BadRequest(new[] { "You cannot change the skill." });
            if (skillFromDb.YearsOfExperience == skill.YearsOfExperience && skillFromDb.ProficiencyLevel == skill.ProficiencyLevel)
                return Ok(skill);

            try
            {
                skillFromDb.Archive.Add(new SkillChange
                {
                    EmployeeSkill = skillFromDb,
                    OldProficiencyLevel = skillFromDb.ProficiencyLevel,
                    OldYearsOfExperience = skillFromDb.YearsOfExperience,
                    NewProficiencyLevel = skill.ProficiencyLevel,
                    NewYearsOfExperience = skill.YearsOfExperience
                });

                skillFromDb.YearsOfExperience = skill.YearsOfExperience;
                skillFromDb.ProficiencyLevel = skill.ProficiencyLevel;
                var employee = db.Employees.SingleOrDefault(x => x.Id == skill.EmployeeId);
                if (employee != null) employee.LastSkillUpdate = DateTime.Now;
                else return BadRequest(new[] { $"There is no such employee with id {skill.EmployeeId}." });

                db.EmployeeSkills.Update(skillFromDb);
                db.Update(employee);
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Error(ex, "Db update failed");
                return StatusCode(409, new[] { "Entity conflict detected." });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Db update failed");
                return BadRequest(new[] { $"DB Update error: {ex.Message}" });
            }

            return Ok(skill);
        }

        public override IActionResult Delete(int id)
        {
            var skill = db.EmployeeSkills.SingleOrDefault(x => x.Id == id);
            if (skill == null) return NotFound();
            if (skill.EmployeeId.ToString() != User.Claims.Single(x => x.Type == "employeeId").Value) return Unauthorized();

            db.EmployeeSkills.Remove(skill);
            db.SaveChanges();

            return Ok();
        }

        [HttpGet("Archive/{id}")]
        public IActionResult GetArchive(int id)
        {
            var archive = db.SkillChanges.Where(x => x.EmployeeSkillId == id).OrderBy(x => x.Timestamp).ToList();
            if (archive == null) return NotFound();

            return Ok(archive);
        }

        [HttpGet("OwnedSkillIds")]
        public IActionResult GetOwnedSkillIds()
        {
            int employeeId = int.Parse(User.Claims.Single(x => x.Type == "employeeId").Value);
            return Json(db.EmployeeSkills.Where(x => x.EmployeeId == employeeId).Select(x => x.SkillId));
        }
    }
}
