﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using XSkills.Web.Models;

namespace XSkills.Web.Api
{
    public class CrudController<T> : Controller where T : EntityBase
    {
        protected readonly XSkillsDbContext db;

        public CrudController(XSkillsDbContext db)
        {
            this.db = db;
        }

        [HttpGet]
        public virtual IEnumerable<T> Get() => db.Set<T>().ToList();

        [HttpPost]
        public virtual IActionResult Post([FromBody] T item)
        {
            if(!ModelState.IsValid) return BadRequest(ModelState.SelectMany(x => x.Value.Errors.Select(y => y.ErrorMessage)));
            if (item == null) return BadRequest(new[] { "Bad data format." });
            if (item.Id != 0) return BadRequest(new[] { "This endpoint is insert-only. Try to PUT instead." });

            try
            {
                db.Set<T>().Add(item);
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Error(ex, "Db insert failed");
                return StatusCode(409, new[] { "Entity conflict detected" });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Db insert failed");
                return BadRequest(new[] { $"An error occured: {ex.Message}" });
            }

            return Ok(item.Id);
        }

        [HttpPut]
        public virtual IActionResult Put([FromBody] T item)
        {
            if(!ModelState.IsValid) return BadRequest(ModelState.SelectMany(x => x.Value.Errors.Select(y => y.ErrorMessage)));
            if (item == null) return BadRequest(new[] { "Bad data format." });
            if (item.Id == 0) return BadRequest(new[] { "This endpoint is update-only. Try to PUT instead." });

            try
            {
                db.Set<T>().Update(item);
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Error(ex, "Db update failed");
                return StatusCode(409, new[] { "Entity conflict detected" });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Db update failed");
                return BadRequest(new[] { $"DB Insert error: {ex.Message}" });
            }

            return Ok(item);
        }

        [HttpDelete("{id}")]
        public virtual IActionResult Delete(int id)
        {
            var item = db.Set<T>().SingleOrDefault(x => x.Id == id);
            if (item == null) return NotFound();

            db.Set<T>().Remove(item);
            db.SaveChanges();

            return Ok();
        }
    }
}
