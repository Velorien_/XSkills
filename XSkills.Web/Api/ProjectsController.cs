﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Linq;
using XSkills.Web.Models;

namespace XSkills.Web.Api
{
    [Route("api/Projects")]
    public class ProjectsController : CrudController<Project>
    {
        public ProjectsController(XSkillsDbContext db) : base(db) { }

        [HttpGet("Tag/{projectId}/{tagId}")]
        public IActionResult Tag(int projectId, int tagId)
        {
            try
            {
                db.TagToProjects.Add(new TagToProject { ProjectId = projectId, TagId = tagId });
                db.SaveChanges();
                return Ok((projectId, tagId));
            }
            catch (Exception ex)
            {
                string message = $"Could not tag projectId {projectId} with tagId {tagId}";
                Log.Error(ex, message);
                return BadRequest(new[] { message });
            }
        }

        [HttpGet("Untag/{projectId}/{tagId}")]
        public IActionResult Untag(int projectId, int tagId)
        {
            try
            {
                db.TagToProjects.Remove(db.TagToProjects.Single(x => x.TagId == tagId && x.ProjectId == projectId));
                db.SaveChanges();
                return Ok((projectId, tagId));
            }
            catch (Exception ex)
            {
                string message = $"Could not untag projectId {projectId} with tagId {tagId}";
                Log.Error(ex, message);
                return BadRequest(new[] { message });
            }
        }
    }
}
