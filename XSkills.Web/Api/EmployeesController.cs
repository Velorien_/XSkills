﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using XSkills.Web.Models;

namespace XSkills.Web.Api
{
    [Route("api/Employees")]
    public class EmployeesController : CrudController<Employee>
    {
        public EmployeesController(XSkillsDbContext db) : base(db) { }

        public override IActionResult Put([FromBody] Employee employee)
        {
            if (employee != null && employee.Id.ToString() != User.Claims.Single(x => x.Type == "employeeId").Value) return Unauthorized();
            return base.Put(employee);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var employee = db.Employees.Where(x => x.Id == id).Include(x => x.User).Include(x => x.Skills).ThenInclude(x => x.Skill).ThenInclude(x => x.TagToSkills).ThenInclude(x => x.Tag)
                .Include(x => x.Skills).ThenInclude(x => x.Skill).ThenInclude(x => x.SkillGroup).ToList()
                .Select(x => NamedEmployee.FromEmployee(x)).SingleOrDefault();

            if (employee == null) return NotFound(new[] { $"There's no employee with id {id}" });
            return Ok(employee);
        }

        [HttpGet("ByUser/{id}")]
        public IActionResult GetByUserId(int id)
        {
            var employee = db.Employees.Where(x => x.UserId == id).Include(x => x.User).Include(x => x.Skills).ThenInclude(x => x.Skill).ThenInclude(x => x.TagToSkills).ThenInclude(x => x.Tag)
                .Include(x => x.Skills).ThenInclude(x => x.Skill).ThenInclude(x => x.SkillGroup).ToList()
                .Select(x => NamedEmployee.FromEmployee(x)).SingleOrDefault();
            if (employee == null) return NotFound(new[] { $"There's no employee with userId {id}" });

            return Ok(employee);
        }

        [HttpGet("Search/{name}")]
        public IActionResult GetByName(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return Ok(Enumerable.Empty<object>());

            name = name.ToLower();
            var employees = db.Employees.Include(x => x.User)
                .Where(x => x.User.FirstName.ToLower().Contains(name) || x.User.LastName.ToLower().Contains(name))
                .Select(x => new { x.User.FirstName, x.User.LastName, x.User.Email, x.Id, x.User.Avatar, x.UserId }).ToList();
            return Ok(employees);
        }

        [HttpGet("SetAssignmentStatus")]
        [Authorize(Roles = "superadmin,admin")]
        public IActionResult SetAssignmentStatus(int id, AssignmentStatus assignmentStatus)
        {
            var user = db.Users.SingleOrDefault(x => x.Id == id);
            if (user == null) return NotFound(new[] { $"There's no employee with userId {id}" });
            user.AssignmentStatus = assignmentStatus;
            db.SaveChanges();

            return Ok();
        }

        [NonAction]
        public override IActionResult Delete(int id) => default;

        [NonAction]
        public override IActionResult Post([FromBody] Employee item) => default;
    }
}
