﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace XSkills.Web.Models
{
    public class SkillChange : EntityBase
    {
        public SkillChange()
        {
            Timestamp = DateTime.Now;
        }

        [JsonIgnore]
        public EmployeeSkill EmployeeSkill { get; set; }
        public int EmployeeSkillId { get; set; }

        public double? OldYearsOfExperience { get; set; }

        public double? OldProficiencyLevel { get; set; }

        public double NewYearsOfExperience { get; set; }

        public double NewProficiencyLevel { get; set; }

        public DateTime Timestamp { get; set; }

        [NotMapped]
        public string Date => Timestamp.ToString("yyyy-MM-dd HH:mm");
    }
}
